#!/usr/bin/env sh

# install dependencies
apt-get update && apt-get install -y jq zip
# create release version
npm --no-git-tag-version version patch
RELEASE_VERSION=$(jq -r '.version' package.json)

# upload assets to package
BUILD_ASSET_LOCAL_DIR="dist/release"
mkdir -p $BUILD_ASSET_LOCAL_DIR
BUILD_ASSET_LOCAL="${BUILD_ASSET_LOCAL_DIR}/${RELEASE_VERSION}.zip"
BUILD_ASSET_LINK="https://gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/raw/${BUILD_ASSET_LOCAL}"
zip -rvj $BUILD_ASSET_LOCAL dist/gitlab-frontend/*
echo "Check if zip file created"
ls -la dist/release/

# reset scripts to old state
git checkout HEAD -- scripts
git config --global user.name "$GITLAB_USER_NAME"
git config --global user.email "$GITLAB_USER_EMAIL"
git remote set-url origin "https://gitlab-ci-token:$GIT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
git commit -am "CI: release $RELEASE_VERSION [skip ci]"
git push origin HEAD:${CI_COMMIT_REF_NAME}

# publish environment variables in
echo "export RELEASE_VERSION=$RELEASE_VERSION" >> $VARIABLES_FILE
echo "export BUILD_ASSET_LOCAL=$BUILD_ASSET_LOCAL" >> $VARIABLES_FILE
echo "export BUILD_ASSET_LINK=$BUILD_ASSET_LINK" >> $VARIABLES_FILE